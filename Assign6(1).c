#include <stdio.h>

void pattern(int a);
void row(int b);
int numrows=1;
int x;

void pattern(int a)
{
    if(a>0)
    {
     row(numrows);
     printf("\n");
     numrows++;
     pattern(a-1);
    }
}

void row(int b)
{
    if(b>0)
    {
    printf("%d",b);
    row(b-1);
    }
}

int main()
{
 printf("Enter the row numbers:");
 scanf("%d",&x);
 pattern(x);
 return 0;
}
